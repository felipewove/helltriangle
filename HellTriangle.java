public class HellTriangle {

    public static void main(String[] args) {
        int [][]triangulo_exemplo = {{6}, 
                                     {3, 5}, 
                                     {9, 7, 1}, 
                                     {4, 6, 8, 9}}; //triangulo do exemplo
        int [][]triangulo_um = {{4},
                                {3,3}};
        int [][]triangulo_dois = {{}}; //nulo
        int [][]triangulo_tres = {}; //nulo
        int [][]triangulo_quatro = null;
        int [][]triangulo_cinco = {{6}};
        int [][]triangulo_seis = {{2},
                                  {3, 5}};
        int [][]triangulo_sete = {{2},
                                  {3, 5},
                                  {10, 5, 2}};
        int [][]triangulo_oito = {{2, 2}};
        int [][]triangulo_nove = {{1},
                                  {2}};
        int [][]triangulo_dez = {{1},
                                 {2,5,6}};

        /*
        System.out.println("Triangulo nulo {{}}: "+maximoTotal(triangulo_dois)+" | Resultado esperado: -2 (Triangulo com linha(s) nula(s)");
        System.out.println("Triangulo nulo {}: "+maximoTotal(triangulo_tres)+" | Resultado esperado: -1 (Triangulo sem linha)");
        System.out.println("Triangulo não inicializado: "+maximoTotal(triangulo_quatro)+" | Resultado esperado: -3 (valor null)");
        System.out.println("Triangulo com apenas um valor: "+maximoTotal(triangulo_cinco)+" | Resultado esperado: 6");
        System.out.println("Triangulo com duas linhas: "+maximoTotal(triangulo_seis)+" | Resultado esperado: 7");
        System.out.println("Triangulo com tres linhas: "+maximoTotal(triangulo_sete)+" | Resultado esperado: 15");
        System.out.println("Triangulo {{2,2}}: "+maximoTotal(triangulo_oito)+" | Resultado esperado: -4 (dois valores na ponta)");
        System.out.println("Triangulo {{1},{2}}: "+maximoTotal(triangulo_nove)+" | Resultado esperado: -5 (triangulo possui linha incompleta)");
        System.out.println("Triangulo {{1},{2}}: "+maximoTotal(triangulo_dez)+" | Resultado esperado: -6 (triangulo possui linha com numero(s) a mais)");

        System.out.println("Triangulo do exemplo: "+maximoTotal(triangulo_exemplo)+" | Resultado esperado: 26");
        */
        
        System.out.println(testFunction(triangulo_um, 7));
        System.out.println(testFunction(triangulo_dois, -2));
        System.out.println(testFunction(triangulo_tres, -1));
        System.out.println(testFunction(triangulo_quatro, -3));
        System.out.println(testFunction(triangulo_cinco, 6));
        System.out.println(testFunction(triangulo_seis, 7));
        System.out.println(testFunction(triangulo_sete, 15));
        System.out.println(testFunction(triangulo_oito, -4));
        System.out.println(testFunction(triangulo_nove, -5));
        System.out.println(testFunction(triangulo_dez, -6));
        System.out.println(testFunction(triangulo_exemplo, 26));
    }
    
    static int [][]triangulo_global = null;
    static int max = 0;
        
    public static int maximoTotal(int  [][]triangulo){
        if (triangulo == null) {
            return -3; //inicializacao nula
        }
        
        //torna triangulo global para manter uma unica copia em memoria
        //a cada iteracao, limpa os valores armazenados da memoria
        triangulo_global = triangulo.clone();
        max = 0;
        
        if (triangulo.length == 0) {
            return -1; //triangulo nulo
        }
        if (triangulo[0].length == 0) {
            return -2; //triangulo com linha nula
        }
        if (triangulo[0].length > 1) {
            return -4; //triangulo com 'dupla ponta'
        }
        for (int i = 0; i < triangulo.length; i++) {
            if (triangulo_global[i].length < i+1) {
                return -5; //triangulo possui linha incompleta
            }
            if (triangulo_global[i].length > i+1) {
                return -6; //triangulo possui linha com numero(s) a mais
            }
        }

        //inicializa com ponta da piramide
        getValue(1,0,triangulo[0][0]); //caminha pela esquerda
        getValue(1,1,triangulo[0][0]); //caminha pela direita
        
        return max;
    }

    public static void getValue(int i, int j, int soma){
        if(i == triangulo_global.length){
            if (soma > max) max = soma;
        }
        else {
            getValue(i+1, j, soma+triangulo_global[i][j]); //proxima linha e valor a esquerda
            getValue(i+1, j+1, soma+triangulo_global[i][j]); //proxima linha e valor a direita
        }
    }

    public static String testFunction(int [][]t, int esperado){
        if (maximoTotal(t) == esperado) {
            return "ok";
        }
        else return "fail";
    }
    
}